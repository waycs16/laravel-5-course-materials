<?php namespace App\Http\Controllers;

use App\User;

class WelcomeController extends Controller {
	public function __construct()
	{
		$this->middleware('guest');
	}
	public function index()
	{
		$user = User::first();
		return view('welcome')->withUser($user);
	}

}
