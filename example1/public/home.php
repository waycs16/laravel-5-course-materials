<?php 
$dir = 'sqlite:database.sqlite';
$sqlite  = new PDO($dir) or die("cannot open the database");
$query =  "SELECT * FROM users LIMIT 0,1";
$statement = $sqlite->prepare($query);
$statement->execute();
$user = null;
$rows = $statement->fetchAll();

foreach ($rows as $row)
{
    $user = $row;
    break;
}

$quotes = [

	'When there is no desire, all things are at peace. - Laozi',
	'Simplicity is the ultimate sophistication. - Leonardo da Vinci',
	'Simplicity is the essence of happiness. - Cedric Bledsoe',
	'Smile, breathe, and go slowly. - Thich Nhat Hanh',
	'Simplicity is an acquired taste. - Katharine Gerould',
	'Well begun is half done. - Aristotle',

];
?>
<html>
	<head>
		<title>Laravel</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #555;
				display: table;
				font-weight: 500;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				color: #000;
				font-size: 24px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<h1>Hello <u><?php echo $user['name']; ?></u> !!</h1>
				<div class="title">Laravel 5</div>
				<div class="quote"><?php echo $quotes[rand(0,5)]; ?></div>
			</div>
		</div>
	</body>
</html>
